﻿using System;
using System.Collections.Generic;
using System.Linq;
using AutoFixture;
using AutoFixture.AutoMoq;
using FluentAssertions;
using Microsoft.AspNetCore.Mvc;
using Moq;
using Otus.Teaching.PromoCodeFactory.Core.Abstractions.Repositories;
using Otus.Teaching.PromoCodeFactory.Core.Domain.PromoCodeManagement;
using Otus.Teaching.PromoCodeFactory.WebHost.Controllers;
using Otus.Teaching.PromoCodeFactory.WebHost.Models;
using Xunit;

namespace Otus.Teaching.PromoCodeFactory.UnitTests.WebHost.Controllers.Partners
{
    public class SetPartnerPromoCodeLimitAsyncTests
    {
        //TODO: Add Unit Tests
        private readonly Mock<IRepository<Partner>> _partnersRepositoryMock;
        private readonly PartnersController _partnersController;

        public SetPartnerPromoCodeLimitAsyncTests()
        {
            var fixture = new Fixture().Customize(new AutoMoqCustomization());
            _partnersRepositoryMock = fixture.Freeze<Mock<IRepository<Partner>>>();
            _partnersController = fixture.Build<PartnersController>().OmitAutoProperties().Create();
        }
        
        public Partner CreateBasePartner()
        {
            var partner = new Partner()
            {
                Id = Guid.Parse("7d994823-8226-4273-b063-1a95f3cc1df8"),
                Name = "Суперигрушки",
                IsActive = true,
                PartnerLimits = new List<PartnerPromoCodeLimit>()
                {
                    new PartnerPromoCodeLimit()
                    {
                        Id = Guid.Parse("e00633a5-978a-420e-a7d6-3e1dab116393"),
                        CreateDate = new DateTime(2020, 07, 9),
                        EndDate = new DateTime(2020, 10, 9),
                        Limit = 100
                    }
                }
            };

            return partner;
        }

        /// <summary>
        /// Если партнер не найден, то также нужно выдать ошибку 404;
        /// </summary>
        [Fact]
        public async void SetPartnerPromoCodeLimit_PartnerNotFound_Get404()
        {
            // Arrange
            var partnerId = Guid.Parse("def47943-7aaf-44a1-ae21-05aa4948b165");
            Partner partner = null;

            _partnersRepositoryMock.Setup(repo => repo.GetByIdAsync(partnerId))
                .ReturnsAsync(partner);

            var setPartnerPromoCodeLimitRequest = new Fixture().Create<SetPartnerPromoCodeLimitRequest>();
            
            // Act
            var result = await _partnersController.SetPartnerPromoCodeLimitAsync(partnerId, setPartnerPromoCodeLimitRequest);
            
            // Assert
            result.Should().BeAssignableTo<NotFoundResult>();
        }
        
        /// <summary>
        /// Если партнер заблокирован, то есть поле IsActive=false в классе Partner, то также нужно выдать ошибку 400;
        /// </summary>
        [Fact]
        public async void SetPartnerPromoCodeLimit_PartnerBlock_Get404()
        {
            // Arrange
            var partnerId = Guid.Parse("def47943-7aaf-44a1-ae21-05aa4948b165");
            var partner = CreateBasePartner();
            partner.IsActive = false;
            
            _partnersRepositoryMock.Setup(repo => repo.GetByIdAsync(partnerId))
                .ReturnsAsync(partner);
            
            var setPartnerPromoCodeLimitRequest = new Fixture().Create<SetPartnerPromoCodeLimitRequest>();

            // Act
            var result = await _partnersController.SetPartnerPromoCodeLimitAsync(partnerId, setPartnerPromoCodeLimitRequest);
 
            // Assert
            result.Should().BeAssignableTo<BadRequestObjectResult>();
        }
        
        /// <summary>
        /// Если партнеру выставляется лимит, то мы должны обнулить количество промокодов, которые партнер выдал
        /// NumberIssuedPromoCodes, если лимит закончился, то количество не обнуляется;
        /// </summary>
        [Fact]
        public async void SetPartnerPromoCodeLimit_PartnerHaveLimit_NumberIssuedPromoCodesIs0()
        {
            // Arrange
            var partnerId = Guid.Parse("def47943-7aaf-44a1-ae21-05aa4948b165");
            var partner = CreateBasePartner();
            
            partner.PartnerLimits = new List<PartnerPromoCodeLimit>
            {
                new PartnerPromoCodeLimit()
                {
                    Id = Guid.Parse("A2998FA2-ED6A-44E8-A715-77F517B66B9A"),
                    CreateDate = new DateTime(2023, 02, 22),
                    EndDate = new DateTime(2023, 12, 9),
                    Limit = 100
                }
            };
            partner.NumberIssuedPromoCodes = 50;

            
            _partnersRepositoryMock.Setup(repo => repo.GetByIdAsync(partnerId))
                .ReturnsAsync(partner);
            
            
            var setPartnerPromoCodeLimitRequest = new Fixture().Create<SetPartnerPromoCodeLimitRequest>();

            // Act
            var result = await _partnersController.SetPartnerPromoCodeLimitAsync(partnerId, setPartnerPromoCodeLimitRequest);
 
            // Assert
            partner.NumberIssuedPromoCodes.Should().Be(0);

        }
        
        /// <summary>
        /// Если партнеру выставляется лимит, то мы должны обнулить количество промокодов, которые партнер выдал
        /// NumberIssuedPromoCodes, если лимит закончился, то количество не обнуляется;
        /// </summary>
        [Fact]
        public async void SetPartnerPromoCodeLimit_PartnerHaveLimitandlimitIsEnd_NumberIssuedPromoCodesNotBe0()
        {
            // Arrange
            var partnerId = Guid.Parse("def47943-7aaf-44a1-ae21-05aa4948b165");
            var partner = CreateBasePartner();
            
            partner.PartnerLimits = new List<PartnerPromoCodeLimit>
            {
                new PartnerPromoCodeLimit()
                {
                    Id = Guid.Parse("A2998FA2-ED6A-44E8-A715-77F517B66B9A"),
                    CreateDate = new DateTime(2023, 02, 22),
                    EndDate = new DateTime(2023, 12, 9),
                    Limit = 100,
                    CancelDate = new DateTime(2023, 12, 9),
                }
            };
            partner.NumberIssuedPromoCodes = 50;

            
            _partnersRepositoryMock.Setup(repo => repo.GetByIdAsync(partnerId))
                .ReturnsAsync(partner);
            
            
            var setPartnerPromoCodeLimitRequest = new Fixture().Create<SetPartnerPromoCodeLimitRequest>();

            // Act
            var result = await _partnersController.SetPartnerPromoCodeLimitAsync(partnerId, setPartnerPromoCodeLimitRequest);
 
            // Assert
            partner.NumberIssuedPromoCodes.Should().NotBe(0);

        }
        
        /// <summary>
        /// При установке лимита нужно отключить предыдущий лимит
        /// </summary>
        [Fact]
        public async void SetPartnerPromoCodeLimit_SetLimit_CancelDateHaveValue()
        {
            // Arrange
            var partnerId = Guid.Parse("def47943-7aaf-44a1-ae21-05aa4948b165");
            var partner = CreateBasePartner();
            partner.IsActive = true;
            
            partner.PartnerLimits = new List<PartnerPromoCodeLimit>
            {
                new PartnerPromoCodeLimit()
                {
                    Id = Guid.Parse("A2998FA2-ED6A-44E8-A715-77F517B66B9A"),
                    CreateDate = new DateTime(2023, 02, 22),
                    EndDate = new DateTime(2023, 12, 9),
                    Limit = 100
                }
            };
            partner.NumberIssuedPromoCodes = 50;
            
            var setPartnerPromoCodeLimitRequest = new Fixture().Create<SetPartnerPromoCodeLimitRequest>();
            setPartnerPromoCodeLimitRequest.Limit = 10;
            
            var currentLimit = partner.PartnerLimits.First();
            
            _partnersRepositoryMock.Setup(repo => repo.GetByIdAsync(partnerId))
                .ReturnsAsync(partner);

            // Act
            await _partnersController.SetPartnerPromoCodeLimitAsync(partnerId, setPartnerPromoCodeLimitRequest);
 
            // Assert
            currentLimit.CancelDate.Should().HaveValue();
        }
        
        /// <summary>
        /// Лимит должен быть больше 0
        /// </summary>
        [Fact]
        public async void SetPartnerPromoCodeLimit_LimitIs0_BadRequest()
        {
            // Arrange
            var partnerId = Guid.Parse("def47943-7aaf-44a1-ae21-05aa4948b165");
            var partner = CreateBasePartner();
            partner.IsActive = true;
            
            partner.PartnerLimits = new List<PartnerPromoCodeLimit>
            {
                new PartnerPromoCodeLimit()
                {
                    Id = Guid.Parse("A2998FA2-ED6A-44E8-A715-77F517B66B9A"),
                    CreateDate = new DateTime(2023, 02, 22),
                    EndDate = new DateTime(2023, 12, 9),
                    Limit = 100
                }
            };
            partner.NumberIssuedPromoCodes = 50;
            
            var setPartnerPromoCodeLimitRequest = new Fixture().Create<SetPartnerPromoCodeLimitRequest>();
            setPartnerPromoCodeLimitRequest.Limit = 0;
            
            _partnersRepositoryMock.Setup(repo => repo.GetByIdAsync(partnerId))
                .ReturnsAsync(partner);

            // Act
            var result =  await _partnersController.SetPartnerPromoCodeLimitAsync(partnerId, setPartnerPromoCodeLimitRequest);
 
            // Assert
            result.Should().BeAssignableTo<BadRequestObjectResult>();
        }
    }
}